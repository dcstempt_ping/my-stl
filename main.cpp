#include <iostream>
#include <cxxabi.h>     // 输出自定义类型名称所需头文件
#include "set_test.h"
#include "set.h"


int main()
{
    // algo_test();
    // test::vector_test::vector_test();
    // test::list_test::list_test();
    // test::deque_test::deque_test();
    // test::stack_test::stack_test();
    // test::queue_test::queue_test();
    // test::queue_test::priority_queue_test();
    // test::rb_tree_test::rb_tree_test();
    test::set_test::multiset_test();

    // cin.get();

    // std::cout << "-|-----------------|-----------------|-" << std::endl;
    // std::cout << "-|--                               --|-" << std::endl;
    // std::cout << "-|--       set initializing        --|-" << std::endl;
    // std::cout << "-|--                               --|-" << std::endl;
    // std::cout << "-|-----------------|-----------------|-" << std::endl;

    

    return 0;
}
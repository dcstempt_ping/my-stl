#include "rb_tree.h"
#include "vector.h"

void display(mystl::rb_tree<int, mystl::less<int>> tree_x)
{
    for (mystl::rb_tree<int, mystl::less<int>>::iterator it = tree_x.begin(); it != tree_x.end(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;
}

namespace test
{
namespace rb_tree_test
{
    void rb_tree_test()
    {
        mystl::rb_tree<int, mystl::less<int>> a;
    
        a.emplace_multi(10);
        mystl::rb_tree<int, mystl::less<int>> b(a);
        mystl::rb_tree<int, mystl::less<int>> c(std::move(a));

        auto result_a = b.emplace_unique(12);
        b.emplace_unique(8);
        b.emplace_unique(15);
        b.emplace_unique(9);
        b.emplace_multi_use_hint(b.begin()++, 13);
        b.emplace_unique_use_hint(b.begin(), 18);
        b.insert_multi(6);
        b.insert_multi(b.begin()++, 6);

        mystl::vector<int> x = {23, 54, 67, 51, 41, 98, 16, 4};
        b.insert_multi(x.begin(), x.end());
        display(b);
        b.insert_unique(x.begin(), x.end());
        b.erase(b.begin());
        display(b);
        auto erase_num = b.erase_multi(6);
        // std::cout << erase_num << std::endl;
        display(b);
        b.erase_unique(8);
        display(b);

        auto k = b.find(12);
        b.emplace_multi(23);
        b.emplace_multi(23);
        b.emplace_multi(23);
        display(b);
        std::cout << *k << std::endl;
        std::cout << b.count_multi(23) << std::endl;
        std::cout << b.count_unique(23) << std::endl;

        int f = 51;
        auto result_c = b.equal_range_unique(f);
        std::cout << *result_c.first << std::endl;
        std::cout << *result_c.second << std::endl;

        int g = 23;
        auto result_d = b.equal_range_unique(g);
        std::cout << *result_d.first << " " << mystl::distance(b.begin(), result_d.first) << std::endl;
        std::cout << *result_d.second << " " << mystl::distance(b.begin(), result_d.second) << std::endl;
        
        
    }


}   // rb_tree_test
}   // test